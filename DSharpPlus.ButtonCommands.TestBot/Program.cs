﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using DSharpPlus.ButtonCommands.Extensions;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DSharpPlus.ButtonCommands.TestBot
{
	static class Program
	{
		private static async Task Main()
		{
			string token = Environment.GetEnvironmentVariable("TOKEN");
			if (token == null)
				Console.WriteLine(
					"Please set the 'TOKEN' environment variable to a valid Discord token and then restart this executable");
			
			DiscordClient client = new(new DiscordConfiguration
			{
				Token = token
			});

			CommandsNextExtension commandsNext = client.UseCommandsNext(new CommandsNextConfiguration
			{
				CaseSensitive = false,
				DmHelp = false,
				EnableDms = false,
				StringPrefixes = new []{"!!"},
				EnableMentionPrefix = true,
				IgnoreExtraArguments = true
			});
			
			commandsNext.RegisterCommands(Assembly.GetExecutingAssembly());
			
			ButtonCommandsExtension buttonCommands = client.UseButtonCommands(new ButtonCommandsConfiguration
			{
				ArgumentSeparator = "-",
				Prefix = "@",
				Services = new ServiceCollection()
					.AddSingleton<Random>()
					.BuildServiceProvider()
			});
			
			buttonCommands.RegisterButtons(Assembly.GetExecutingAssembly());
			buttonCommands.RegisterConverter(new DiscordMessageConverter());

			buttonCommands.ButtonCommandExecuted += (ext, args) =>
			{
				args.Context.Client.Logger.LogInformation(
					$"{args.Context.User} has used button command {args.CommandName} ({args.ButtonId})");
				return Task.CompletedTask;
			};
			buttonCommands.ButtonCommandErrored += (ext, args) =>
			{
				args.Context.Client.Logger.LogError(args.Exception,
					$"{args.Context.Member} has used button command {args.CommandName} ({args.ButtonId}) which threw an exception");
				return Task.CompletedTask;
			};

			client.Ready += (sender, _) =>
			{
				client.Logger.LogInformation(
					$"Connected to Discord as {sender.CurrentUser.Username}#{sender.CurrentUser.Discriminator}");
				client.Logger.LogInformation($"The invite URL is: {client.CurrentApplication.GenerateBotOAuth()}");
				return Task.CompletedTask;
			};

			await client.ConnectAsync(new DiscordActivity("i dont know why im doing this send help"));
			await Task.Delay(-1);
		}
	}
}