using System;
using System.Threading.Tasks;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.TestBot
{
	public class ExampleButtons : ButtonCommandModule
	{
		public Random Random { private get; set; } // Implied public setter.
		
		[ButtonCommand("example")]
		public async Task ExampleButton(ButtonContext context)
		{
			await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
				new DiscordInteractionResponseBuilder().WithContent("You clicked the blue example button!"));
		}
		
		[ButtonCommand("user")]
		public async Task ExampleButton(ButtonContext context, DiscordUser user)
		{
			await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
				new DiscordInteractionResponseBuilder().WithContent($"{Formatter.Mention(context.Member)} has clicked the button that had {Formatter.Mention(user)} as an argument"));
		}
		
		[ButtonCommand("user.string")]
		public async Task ExampleButton(ButtonContext context, DiscordChannel channel, string str)
		{
			await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
				new DiscordInteractionResponseBuilder().WithContent($"{str}, {Formatter.Mention(channel)}"));
		}
		
		[ButtonCommand("message")]
		public async Task ExampleButton(ButtonContext context, DiscordMessage msg)
		{
			await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
				new DiscordInteractionResponseBuilder().WithContent(msg.JumpLink.ToString()));
		}
		
		[ButtonCommand("di")]
		public async Task DependencyInjection(ButtonContext context)
		{
			await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
				new DiscordInteractionResponseBuilder().WithContent(Random.Next().ToString()));
		}
	}
}