# DSharpPlus.ButtonCommands

An extension for DSharpPlus to use buttons like commands from CommandsNext

# Examples

Example code can be found in the `DSharpPlus.ButtonCommands.TestBot` project. You can run that project after setting the `TOKEN` environment variable to a valid bot token.

# Documentation

## Setup

You can register a `ButtonCommandsExtension` on your `DiscordClient`, similar to how you register a `CommandsNextExtension`.

```c#
ButtonCommandsExtension buttonCommands = discord.UseButtonCommands();
```

You can also pass in a `ButtonCommandsConfiguration` if you want to customize the button ID handling.

```c#
ButtonCommandsExtension buttonCommands = discord.UseButtonCommands(new ButtonCommandsConfiguration() {
    ArgumentSeparator = ".",
    Prefix = "@"
});
```

## Creating and registering button commands

Just like CommandsNext, to add button commands, you should create a new class that inherits from `ButtonCommandModule`.

```c#
public class MyButtonCommands : ButtonCommandModule
{ }
```

Now, you can add a method that will be your button command. Make sure to add the `ButtonCommand` attribute on top of it.

```c#
[ButtonCommand("ban")]
public async Task BanMemberButton(ButtonContext context, DiscordMember member)
{ }
```

> Note: Don't forget that button IDs have a 100 character limit, so try not to have long command names, since the arguments will also gonna be stored in that 100 characters.

Now, you can write the actual command code in that method. Don't forget to respond to the interaction.

```c#
[ButtonCommand("ban")]
public async Task BanMemberButton(ButtonContext context, DiscordMember member, string reason)
{
    context.Guild.BanMemberAsync(member, 7, reason);
    await context.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
	    new DiscordInteractionResponseBuilder().WithContent($"{context.Member.Mention} has just banned {member.Username} ({member.Id})!"));
}
```

Now, for this command to get called, you have to register it where you called `UseButtonCommands`. There are 2 ways to register a command module.

```c#
ButtonCommandsExtension buttonCommands = client.UseButtonCommands();

buttonCommands.RegisterButtons<MyButtonCommands>();
```
While this method would work fine, it might cause confusion later on, or might look bad. So, unless you want to have some conditions per module, you can just register the entire assembly.
```c#
ButtonCommandsExtension buttonCommands = client.UseButtonCommands();

buttonCommands.RegisterButtons(Assembly.GetExecutingAssembly());
```

## Using button commands

To use button commands, you can just create any interaction with the id as your command name and arguments, formatted as following

```
<prefix><commandname><separator><parameter1><separator><parameter2>...
```

Example:

```
@ban.310651646302486528
```

Make sure that the prefix and the separators are the ones that you set in the `ButtonCommandsConfiguration`. By default, the prefix is `@` and the separator is `.`.

> Try not to use the prefixes and separators in any of the parameters, since they may cause issues while reading the button ID

Or, you can just use the `SlashCommandsExtension.BuildButtonId` method to generate a button ID for yourself.

Example:

```c#
[Command("ban")]
public async Task BanCommand(CommandContext context, DiscordMember memberToBan, string reason)
{
	ButtonCommandsExtension bc = context.Client.GetButtonCommands();
	DiscordMessageBuilder messageBuilder = new DiscordMessageBuilder()
		.WithContent($"Do you really want to ban {memberToBan.Mention} for **{reason}**?")
		.AddComponents(
			new DiscordButtonComponent(ButtonStyle.Danger, bc.BuildButtonId("ban", memberToBan, reason), "Yes") 
			// BuildButtonId will return '@ban.<id of the memberToBan>.<reason>' 
		);

	await context.RespondAsync(messageBuilder);
}
```

## Custom argument converters

By default, only the following arguments are supported:

- `string`
- `bool`
- `int`
- `uint`
- `long`
- `ulong`
- `float`
- `double`
- `DiscordUser`
- `DiscordMember`
- `DiscordRole`
- `DiscordChannel`

If you want to add more arguments, you can create a new argument converter like so:

```c#
public class ShortConverter : IButtonArgumentConverter<short>
{
    // This method will convert the string from the buttons id to a `short`
    public async Task<Optional<short>> ConvertAsync(string value, ButtonContext ctx)
    {
        return short.TryParse(value, out short result) ? Optional.FromValue(result) : Optional.FromNoValue<short>();
    }

    // This method will convert a `short` to a string to be used in the buttons id
    // `BuildButtonId` will use this method to generate a valid button id
    public string ConvertToString(short value) => value.ToString();
}
```

> Make sure the output of `ConvertToString` can be parsed by `ConvertAsync`!

After writing your argument converter, you can register it in your `ButtonCommandsExtension` like so:

```c#
buttonCommands.RegisterConverter(new ShortConverter());
```