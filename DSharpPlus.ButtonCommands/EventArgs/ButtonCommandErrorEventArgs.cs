using System;
using Emzi0767.Utilities;

namespace DSharpPlus.ButtonCommands.EventArgs
{
	public class ButtonCommandErrorEventArgs : AsyncEventArgs
	{
		public string ButtonId { get; set; }
		public string CommandName { get; set; }
		public ButtonContext Context { get; set; }
		public Exception Exception { get; set; }
	}
}