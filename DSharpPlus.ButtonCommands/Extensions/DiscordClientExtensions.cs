namespace DSharpPlus.ButtonCommands.Extensions
{
	public static class DiscordClientExtensions
	{
		public static ButtonCommandsExtension UseButtonCommands(this DiscordClient client, 
			ButtonCommandsConfiguration config = null)
		{
			ButtonCommandsExtension ext = new(config ?? new ButtonCommandsConfiguration());
			
			client.AddExtension(ext);

			return ext;
		}

		public static ButtonCommandsExtension GetButtonCommands(this DiscordClient client) =>
			client.GetExtension<ButtonCommandsExtension>();
	}
}