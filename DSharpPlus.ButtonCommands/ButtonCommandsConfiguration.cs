using System;
using Microsoft.Extensions.DependencyInjection;

namespace DSharpPlus.ButtonCommands
{
	public class ButtonCommandsConfiguration
	{
		public string ArgumentSeparator = ".";
		public string Prefix = "@";
		public IServiceProvider Services { internal get; set; } = new ServiceCollection().BuildServiceProvider(true);
	}
}