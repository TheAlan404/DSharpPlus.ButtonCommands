using System.Reflection;

namespace DSharpPlus.ButtonCommands
{
	public class ButtonCommand
	{
		public string Name { get; set; }
		public MethodInfo Method { get; set; }
	}
}